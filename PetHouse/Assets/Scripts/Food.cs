﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : ClaimableObject {
	public float remaining = 500f;
	public Tile tile;

	void Start() {
		owner = null;
	}

	public void Eat(float amount) {
		remaining -= amount;

		if (remaining <= 0) {
			UnClaim(owner);
			World.instance.objectManager.foods.Remove(this);
			Destroy(gameObject);
		}
	}
}
