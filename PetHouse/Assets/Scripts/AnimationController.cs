﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
	public ParticleSystem sleepParticles;
	public GameObject sleepBubble;
	public StateController stateController;

	void Start() {
		EndSleep();
		sleepBubble.SetActive(false);
	}

	void Update() {
		sleepBubble.SetActive(stateController.sleepNeed.current < stateController.sleepNeed.max / 2);
	}

	public void StartSleep() {
		sleepBubble.SetActive(false);
		sleepParticles.gameObject.SetActive(true);
		sleepParticles.Play();
	}

	public void EndSleep() {
		sleepParticles.gameObject.SetActive(false);
		sleepParticles.Stop();
	}

	public void KillMe() {
		Destroy(transform.parent.gameObject);
	}
}
