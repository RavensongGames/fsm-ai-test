﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData : MonoBehaviour {
	public static int mapWidth = 25;
	public static int mapHeight = 4;
	public Tile[,] tiles;
	public GameObject tileObject;
	public Sprite[] sprites;

	public void BuildMap() {
		tiles = new Tile[mapWidth, mapHeight];

		for (int y = 0; y < mapHeight; y++) {

			for (int x = 0; x < mapWidth; x++) {
				GameObject g = Instantiate(tileObject, new Vector3(x, y, 0), Quaternion.identity, transform);
				SpriteRenderer sr = g.GetComponent<SpriteRenderer>();
				sr.sortingOrder = -y;
				tiles[x, y] = new Tile(g, x, y);
			}
		}

		Camera.main.orthographicSize = mapHeight / 2f;
		Camera.main.transform.position = new Vector3(mapWidth / 2f, mapHeight / 2f - 1, -10);
	}

	public void PlaceRoom(int x, int y) {
		SpriteRenderer sr  = tiles[x, y].obj.GetComponent<SpriteRenderer>();
		sr.sprite = sprites[4];
		sr.color = new Color(1f, 1f, 1f, 0.25f);

		Job j = new Job(new Vector2(x, y), (job) => { tiles[x, y].room = new Room(x, y); sr.color = Color.white; Autotile(x, y); }, 3f);
		World.instance.jobQueue.Enqueue(j);
	}

	void Autotile(int x, int y) {
		for (int mx = -1; mx <= 1; mx++) {
			int nx = x + mx;

			if (nx < 0 || nx >= mapWidth || tiles[nx, y].room == null)
				continue;

			tiles[nx, y].obj.GetComponent<SpriteRenderer>().sprite = sprites[RoomsAdjacent(nx, y)];
		}
	}

    int RoomsAdjacent(int x, int y) {
        bool E = x < mapWidth - 1 && tiles[x + 1, y].room != null;
        bool W = x > 0 && tiles[x - 1, y].room != null;

        if (!E && !W)
            return 4;
        else if (E && W)
            return 5;

        return (E) ? 0 : 1;
    }

    public bool CanPlace(int x, int y) {
		if (x < 0 || y < 0 || x >= mapWidth || y >= mapHeight)
			return false;

		if (y == 0)
			return true;

		return (tiles[x, y - 1].room != null);
	}

	public static bool OutOfMap(int x, int y) {
		return (x < 0 || y < 0 || x >= mapWidth || y >= mapHeight);
	}
}
