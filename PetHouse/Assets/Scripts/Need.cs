﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Need {
	public float current;
	public float max;

	public Need(float _cur, float _max) {
		current = _cur;
		max = _max;
	}

	public void Update(float deltaTime) {
		if (current >= deltaTime)
			current -= deltaTime;
	}
}
