﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;

public static class Pathfinding {
	public class Path {
		public Tile startTile;
		public Tile endTile;

		//Queue<Tile> path;

		public Path(Tile start, Tile end) {
			startTile = start;
			endTile = end;
			//path = new Queue<Tile>();
			Calculate();
		}

		void Calculate() {
			Queue<Tile> openSet = new Queue<Tile>();
			//SimplePriorityQueue<Tile> closedSet = new SimplePriorityQueue<Tile>();

			openSet.Enqueue(startTile);


		}
	}

	public class Grid {
		public Node[,] nodes;
		public int nodesPerUnit = 2;

		public Grid(int width, int height, int _nodesPerUnit) {
			nodesPerUnit = _nodesPerUnit;
			nodes = new Node[width * nodesPerUnit, height * nodesPerUnit];

			for (int x = 0; x < nodes.GetLength(0); x++) {
				for (int y = 0; y < nodes.GetLength(1); y++) {
					nodes[x, y] = new Node(new Vector2(x / nodesPerUnit, y / nodesPerUnit), x, y, false, 0);
				}
			}
		}
	}
		
	public class Edge {
		public Tile tile;
		public float costToEnter;
	}

	public class Node {
		public Vector2 position;
		public int gridX, gridY;
		public bool walkable;
		public int movePenalty;

		public Node parentNode;
		public int hCost, gCost;

		public int fCost {
			get { return gCost + hCost; }
		}

		public Node(Vector2 _position, int _gridX, int _gridY, bool _walkable, int _movePenalty) {
			walkable = _walkable;
			gridX = _gridX;
			gridY = _gridY;
			walkable = _walkable;
			movePenalty = _movePenalty;
		}

		public int CompareTo(Node other) {
			int compare = fCost.CompareTo(other.fCost);

			if (compare == 0)
				compare = hCost.CompareTo(other.hCost);

			return -compare;
		}
	}
}

public class Tile {
	public int X, Y;
	public GameObject obj;
	public Room room;

	public Tile(GameObject _obj, int x, int y) {
		obj = _obj;
		X = x;
		Y = y;
		room = null;
	}
}
