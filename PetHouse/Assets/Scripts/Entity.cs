﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {
	public SpriteRenderer spriteRenderer;
	public StateController stateController;
	public bool friendly = true;
	public bool initialized = false;

	public Vector2 currentPosition {
		get { return transform.position; }
		set { transform.position = value; }
	}

	public void Move(Vector2 targetPos, float speedMod) {
		transform.position = Vector2.MoveTowards(currentPosition, targetPos, stateController.stats.Speed * Time.deltaTime * 0.1f * speedMod);
	}
}
