﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Job {
	public Vector2 position;

	float jobTime;
	Action<Job> onComplete;
	Action<Job> onCancel;

	public Job(Vector2 pos, Action<Job> complete, float jt = 1f) {
		position = pos;
		onComplete += complete;
		jobTime = jt;
	}

	public void RegisterCompleteCB(Action<Job> complete) {
		onComplete += complete;
	}

	public void UnregisterCompleteCB(Action<Job> complete) {
		onComplete -= complete;
	}

	public void RegisterCancelCB(Action<Job> cancel) {
		onCancel += cancel;
	}

	public void UnregisterCancelCB(Action<Job> cancel) {
		onCancel -= cancel;
	}

	public void DoWork(float deltaTime) {
		jobTime -= deltaTime;

		if (jobTime <= 0)
			onComplete(this);
	}

	public void Cancel() {
		onCancel(this);
	}
}
