﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {
	public State hitState;

	StateController controller;
	int health;
	float speed;

	public int Health {
		get { return health; }
	}

	public float Speed {
		get { return speed; }
	}

	void Start() {
		controller = GetComponent<StateController>();
		health = 100;
		speed = Random.Range(8f, 11f);
	}

	void Update() {
		if (controller.currentState == hitState) {
			controller.entity.Move(controller.entity.currentPosition + controller.hitDir, 2f);
		}
	}

	public void TakeDamage(Entity attacker, int amount) {
		if (health <= 0)
			return;

		controller.target = attacker;
		
		if (controller.currentState != hitState) {
			health -= amount;
			controller.ChangeState(hitState);

			if (attacker.currentPosition.x != controller.entity.currentPosition.x)
				controller.hitDir.x = controller.entity.currentPosition.x < attacker.currentPosition.x ? -.5f : .5f;
		} else {
			controller.hitDir = new Vector2(0, 0);
		}
	}
}

public class BaseStat {
    public int amount { get; protected set; }
    List<int> changes;

    public int total {
        get { return amount + changeAmount; }
    }

    public int changeAmount {
        get {
            int a = 0;

            for (int i = 0; i < changes.Count; i++) {
                a += changes[i];
            }

            return a;
        }
    }

    public BaseStat(int am) {
        amount = am;
        changes = new List<int>();
    }
}
