﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour {
	public GameObject character;
	public GameObject bed;
	public GameObject food;

	public List<Bed> beds { get; protected set; }
	public List<Food> foods { get; protected set; }
	public List<Entity> characters;

	public void Initialize() {
		beds = new List<Bed>();
		foods = new List<Food>();
		characters = new List<Entity>();

		NewBedAt(1, 0);
		NewFoodAt(3, 0);
		NewFoodAt(5, 0);
	}

	public GameObject NewCharacterAt(int x, int y) {
		GameObject g = (GameObject)Instantiate(character, new Vector3(x, y, 0), Quaternion.identity, transform);
		characters.Add(g.GetComponent<Entity>());

		return g;
	}

	public GameObject NewBedAt(int x, int y) {
		GameObject g = (GameObject)Instantiate(bed, new Vector3(x, y, 0), Quaternion.identity, transform);
		Bed b = g.GetComponent<Bed>();

		b.tile = World.instance.map.tiles[x, y];
		beds.Add(b);

		return g;
	}

	public GameObject NewFoodAt(int x, int y) {
		GameObject g = (GameObject)Instantiate(food, new Vector3(x, y, 0), Quaternion.identity, transform);
		Food f = g.GetComponent<Food>();

		f.tile = World.instance.map.tiles[x, y];
		foods.Add(f);

		return g;
	}

	public Bed FindClosestBed(int x, int y) {
		Bed closestBed = null;
		float dist = Mathf.Infinity;

		if (beds.Count <= 0)
			return null;

		foreach (Bed b in beds) {
			float newDist = Vector2.Distance(b.transform.position, new Vector2(x, y));

			if (newDist < dist) {
				dist = newDist;
				closestBed = b;
			}
		}

		return closestBed;
	}
}
