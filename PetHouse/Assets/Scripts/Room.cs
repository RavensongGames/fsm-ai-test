﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room {

	public int X { get; protected set; }  //Left-most tile
	public int Y { get; protected set; } //Bottom-most tile
	public int Width { get; protected set; }
	public int Height { get; protected set; }

	public Room(int x, int y, int w = 1, int h = 1) {
		X = x;
		Y = y;
		Width = w;
		Height = h;
	}
}
