﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Wait For End of State")]
public class WaitForEnd : Decision {
	public override bool Decide(StateController controller) {
		return (controller.timeInState >= controller.timeToTransition);
	}
}
