﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Found Food")]
public class FoundFood : Decision {
	public override bool Decide(StateController controller) {
		if (controller.foodNeed.current > controller.foodNeed.max / 2)
			return false;

		if (World.instance.objectManager.foods.Count <= 0)
			return false;

		Food f = World.instance.objectManager.foods.Find(x => x.owner == controller || x.owner == null);

		if (f != null) {
			f.Claim(controller);
			controller.targetPosition = f.transform.position;
			return true;
		} else {
			return false;
		}
	}
}
