﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/TakenFatalDmg")]
public class TakenFatalDamage : Decision {
	public override bool Decide (StateController controller) {
		if (controller.stats.Health <= 0) {
			return true;
		}

		return false;
	}
}
