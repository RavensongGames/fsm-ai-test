﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Found Bed")]
public class FoundBed : Decision {
	public override bool Decide(StateController controller) {
		if (controller.sleepNeed.current > controller.sleepNeed.max / 2)
			return false;

		if (World.instance.objectManager.beds.Count <= 0)
			return false;

		Bed b = World.instance.objectManager.beds.Find(x => x.owner == controller.entity || x.owner == null);

		if (b != null) {
			b.Claim(controller);
			controller.targetPosition = b.transform.position;
			return true;
		} else {
			return false;
		}
	}
}
