﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Always True")]
public class AlwaysTrue : Decision {
	public override bool Decide (StateController controller) {
		return true;
	}
}
