﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/In Range")]
public class InRangeOfTarget : Decision {
	public override bool Decide(StateController controller) {
		return (controller.attackCooldown <= 0f && Vector2.Distance(controller.entity.currentPosition, controller.targetPosition) < controller.attackDistance);
	}
}
