﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Found Target")]
public class FoundTarget : Decision {
	public override bool Decide(StateController controller) {
		if (controller.target != null)
			return true;

		return PickTarget(controller);
	}

	bool PickTarget(StateController controller) {
		List<Entity> entities = World.instance.objectManager.characters.FindAll(x => 
			x.initialized && 
			x != controller.entity && 
			x.stateController.stats.Health > 0 && 
			controller.entity.friendly != x.friendly);

		if (entities.Count <= 0)
			return false;

		controller.target = entities[Random.Range(0, entities.Count)];

		return true;
	}
}
