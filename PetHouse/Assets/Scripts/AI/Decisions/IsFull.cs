﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Is Full")]
public class IsFull : Decision {
	public override bool Decide(StateController controller) {
		if (controller.inUseObject == null)
			return true;

		if (controller.foodNeed.current >= controller.foodNeed.max) {
			controller.inUseObject.UnClaim(controller);
			return true;
		}

		return false;
	}
}
