﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/At Destination")]
public class AtDestination : Decision {
	public override bool Decide(StateController controller) {
		return (controller.targetPosition == controller.entity.currentPosition);
	}
}