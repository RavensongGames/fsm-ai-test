﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Has Rested")]
public class HasRested : Decision {
	public override bool Decide(StateController controller) {
		if (controller.sleepNeed.current >= controller.sleepNeed.max) {
			controller.inUseObject.UnClaim(controller);
			return true;
		}

		return false;
	}
}