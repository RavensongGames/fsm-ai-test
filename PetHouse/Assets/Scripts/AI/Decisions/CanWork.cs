﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Can Work")]
public class CanWork : Decision {
	public override bool Decide(StateController controller) {
		if (controller.myJob == null) {
			return false;
		} else {
			return true;
		}
	}
}

