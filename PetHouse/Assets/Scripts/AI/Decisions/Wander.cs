﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Wander")]
public class Wander : Decision {
	public override bool Decide(StateController controller) {
        return (controller.timeInState >= controller.timeToTransition);
	}
}
