﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Decisions/Found Job")]
public class FoundJob : Decision {
	public override bool Decide(StateController controller) {
		if (controller.myJob == null && World.instance.jobQueue.Count > 0) {
			controller.myJob = World.instance.jobQueue.Dequeue();
			controller.myJob.RegisterCompleteCB((j) => { controller.myJob = null; } );

			controller.targetPosition = controller.myJob.position;
			return true;
		} else {
			return false;
		}
	}
}
