﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class StateController : MonoBehaviour {
	public State currentState;
	public State previousState;
	public State remainState;
	public Vector2 targetPosition;
	public Need sleepNeed;
	public Need foodNeed;
	public float attackDistance = 0.3f;
	public Entity entity;
	public Stats stats;

	[HideInInspector] public FacingDir facingDir = FacingDir.Right;
	[HideInInspector] public Entity target;
	[HideInInspector] public Job myJob;
	[HideInInspector] public ClaimableObject inUseObject;
	[HideInInspector] public float timeInState;
	[HideInInspector] public float timeToTransition;
	[HideInInspector] public float attackCooldown = 1f;
	public Vector2 hitDir = new Vector2(0f, 0f);

	Animation anim;
	SpriteRenderer spriteRenderer;

	void Start() {
		Initialize();
	}

	void Initialize() {
		entity = GetComponent<Entity>();
		entity.stateController = this;
		stats = GetComponent<Stats>();
		anim = GetComponentInChildren<Animation>();
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		sleepNeed = new Need(Random.Range(190f, 200f), 200f);
		foodNeed = new Need(100f, 100f);
		timeToTransition = Random.Range(0.5f, 3f);
		entity.initialized = true;
        targetPosition = GetRandomPosition();
	}

	void Update() {
		sleepNeed.Update(Time.deltaTime);
		foodNeed.Update(Time.deltaTime);

		if (attackCooldown > 0)
			attackCooldown -= Time.deltaTime;

		currentState.UpdateState(this);
		timeInState += Time.deltaTime;

		spriteRenderer.flipX = (facingDir == FacingDir.Left);
	}

	public bool ChangeState(State state) {
		if (state != remainState) {
			ExitState(currentState);
			EnterState(state);
			return true;
		}

		return false;
	}

	void EnterState(State state) {
		currentState = state;
        timeInState = 0f;
        timeToTransition = Random.Range(0.5f, 3f);

		if (state.animation != null && state.animation != anim.clip) {
			anim.clip = currentState.animation;
			anim.Play(PlayMode.StopAll);
		}
	}

	void ExitState(State state) {
		previousState = state;
	}

    public Vector2 GetRandomPosition() {
        return new Vector2(Random.Range(0, MapData.mapWidth), entity.currentPosition.y);
    }
}

public enum FacingDir {
	Right, Left
}
