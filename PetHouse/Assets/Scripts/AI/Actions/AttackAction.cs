﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Attack")]
public class AttackAction : Action {
	public override void Act(StateController controller) {
		if (controller.attackCooldown <= 0 && Vector2.Distance(controller.entity.currentPosition, controller.targetPosition) < controller.attackDistance) {
			if (controller.target != null) {
				controller.target.stateController.stats.TakeDamage(controller.entity, 10);
				controller.attackCooldown = 1f;
			}
		}
	}
}
