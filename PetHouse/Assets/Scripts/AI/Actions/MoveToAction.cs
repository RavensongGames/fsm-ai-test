﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/MoveTo")]
public class MoveToAction : Action {
	public override void Act(StateController controller) {
		if (controller.entity.currentPosition != controller.targetPosition) {
			MoveTo(controller);
		} else {
			//Wander
			controller.targetPosition = controller.GetRandomPosition();
		}
	}

	void MoveTo(StateController controller) {
		Vector2 curPos = controller.entity.currentPosition;

		if (curPos.y != controller.targetPosition.y) {
			int stairPosX = 0;//World.instance.map.stairPos[(int)curPos.y];

			if (curPos.x == stairPosX) {
				//Move Up Stairs
				Move(controller, new Vector2(curPos.x, controller.targetPosition.y), 0.5f);
			} else {
				//Go To Stairs
				Move(controller, new Vector2(stairPosX, curPos.y));
			}

		} else {
			Move(controller, controller.targetPosition);
		}
	}

	void Move(StateController controller, Vector2 targetPos, float speedMod = 1.0f) {
        Vector2 moveDir = controller.entity.currentPosition - targetPos;
        
		if (moveDir.x != 0)
			controller.facingDir = (moveDir.x < 0) ? FacingDir.Right : FacingDir.Left;

		controller.entity.Move(targetPos, speedMod);
	}
}
