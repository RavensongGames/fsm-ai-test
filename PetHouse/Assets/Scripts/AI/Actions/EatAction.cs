﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Eat")]
public class EatAction : Action {
	public override void Act (StateController controller) {
		float amount = Time.deltaTime * 10f;
		Food f = (Food)controller.inUseObject;
		f.Eat(amount);
		controller.foodNeed.current += amount;
		controller.foodNeed.current = Mathf.Clamp(controller.foodNeed.current, 0, controller.foodNeed.max);
	}
}
