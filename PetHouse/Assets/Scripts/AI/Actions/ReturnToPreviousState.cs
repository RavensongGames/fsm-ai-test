﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Return to Previous State")]
public class ReturnToPreviousState : Action {
	public override void Act(StateController controller) {
		if (controller.timeInState > 0.15f) {
			controller.ChangeState(controller.previousState);
		}
	}
}
