﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Chase")]
public class ChaseAction : Action {
	public override void Act (StateController controller) {
		if (controller.target != null)
			controller.targetPosition = controller.target.currentPosition;

		float dist = Vector2.Distance(controller.entity.currentPosition, controller.targetPosition);

		if (dist > controller.attackDistance)
			ChaseTarget(controller);
	}

	void ChaseTarget(StateController controller) {
		Vector2 curPos = controller.entity.currentPosition;

		if (curPos.y != controller.targetPosition.y) {
			int stairPosX = 0;//World.instance.map.stairPos[(int)curPos.y];

			if (curPos.x == stairPosX) {
				//Move Up Stairs
				Move(controller, new Vector2(curPos.x, controller.targetPosition.y), 0.5f);
			} else {
				//Go To Stairs
				Move(controller, new Vector2(stairPosX, curPos.y));
			}

		} else {
			Move(controller, controller.targetPosition);
		}
	}

	void Move(StateController controller, Vector2 dir, float speedMod = 1.0f) {
		float moveX = controller.entity.currentPosition.x - dir.x;

		if (moveX != 0)
			controller.facingDir = (moveX < 0) ? FacingDir.Right : FacingDir.Left;

		controller.entity.Move(dir, speedMod);
	}
}
