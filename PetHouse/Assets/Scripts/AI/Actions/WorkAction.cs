﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Work Action")]
public class WorkAction : Action {
	public override void Act(StateController controller) {
		if (controller.myJob != null) {
			controller.myJob.DoWork(Time.deltaTime);
			controller.sleepNeed.current -= (Time.deltaTime * 0.5f);
		}
	}
}
