﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/Actions/Sleep")]
public class SleepAction : Action {
	public override void Act (StateController controller) {
		controller.sleepNeed.current += Time.deltaTime * 3f;
	}
}
