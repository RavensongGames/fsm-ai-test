﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "AI/State")]
public class State : ScriptableObject {
	public Action[] actions;
	[Space(10)]
	public Transition[] transitions;
	[Space(10)]
	public AnimationClip animation;

	public void UpdateState(StateController controller) {
		for (int i = 0; i < actions.Length; i++) {
			actions[i].Act(controller);
		}

		for (int i = 0; i < transitions.Length; i++) {
			if (transitions[i].enabled) {
				State state = transitions[i].decision.Decide(controller) ? transitions[i].trueState : transitions[i].falseState;

				if (controller.ChangeState(state))
					break;
			}
		}
	}
}
