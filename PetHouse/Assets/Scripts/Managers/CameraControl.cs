﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
	public Transform cameraTarget;

	Vector3 lastFramePosition;

	void Update() {
		UpdateMouse();
		UpdateKeys();
	}

	void LateUpdate() {
		if (cameraTarget != null)
			transform.position = new Vector3(cameraTarget.position.x, cameraTarget.position.y, transform.position.z);
	}

	void UpdateMouse() {
		Vector3 currFramePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		currFramePosition.z = 0;

		if (Input.GetMouseButton(1)) {
			cameraTarget = null;
			Vector3 diff = lastFramePosition - currFramePosition;
			Camera.main.transform.Translate(diff);
		}

		lastFramePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		lastFramePosition.z = 0;

		if (Input.mouseScrollDelta.y != 0) {
			Camera.main.orthographicSize -= Input.mouseScrollDelta.y * 0.1f;
			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 0.5f, 12f);
		}
	}

	void UpdateKeys() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			World.instance.Pause();
		}
	}
}
