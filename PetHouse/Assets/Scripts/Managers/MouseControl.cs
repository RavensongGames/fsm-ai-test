﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseControl : MonoBehaviour {
	public SpriteRenderer cursor;
	public Color validColor;
	public Color invalidColor;

	CameraControl camControl;
	SelectMode selectMode = SelectMode.None;

	void Start() {
		camControl = FindObjectOfType<CameraControl>();
	}

	void Update() {
		Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		if (selectMode == SelectMode.Build) {
			int x = Mathf.FloorToInt(pos.x + 0.5f), y = Mathf.FloorToInt(pos.y);
			cursor.transform.position = new Vector3(x, y, -1);

			cursor.color = (World.instance.map.CanPlace(x, y)) ? validColor : invalidColor;
		} else {
			cursor.transform.position = new Vector3(-100, -100, 0);
		}

		if (Input.GetMouseButtonDown(0)) {
			Click(pos);
		}

		if (Input.GetKeyDown(KeyCode.Escape) && selectMode == SelectMode.Build) {
			SetMode(0);
		} 
	}

	void Click(Vector2 pos) {
		if (EventSystem.current.IsPointerOverGameObject())
			return;

		RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
		int x = Mathf.FloorToInt(pos.x + 0.5f), y = Mathf.FloorToInt(pos.y);

		if (selectMode == SelectMode.Build) {
			if (World.instance.map.CanPlace(x, y) && World.instance.map.tiles[x, y].room == null)
				World.instance.map.PlaceRoom(x, y);
		} else {
			if (hit.transform != null && hit.transform.GetComponent<Stats>() != null) {
				camControl.cameraTarget = hit.transform;
			} else {
				if (World.instance.map.CanPlace(x, y))
					World.instance.objectManager.NewCharacterAt(x, 0);
			}
		}
	}

	public void SetMode(int mode) {
		selectMode = (selectMode != (SelectMode)mode) ? (SelectMode)mode : SelectMode.None;
	}

	public enum SelectMode {
		None,
		Build
	}
}
