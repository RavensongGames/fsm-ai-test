﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {
	public static World instance;

	public bool paused;
	public Queue<Job> jobQueue;
	public MapData map;
	public ObjectManager objectManager;

	float savedTimeScale = 1f;

	void Start() {
		instance = this;
		jobQueue = new Queue<Job>();

		map = GameObject.FindObjectOfType<MapData>();
		map.BuildMap();
		objectManager = GetComponent<ObjectManager>();
		objectManager.Initialize();
	}

	public void Pause() {
		paused = !paused;

		Time.timeScale = (paused) ? 0.0f : savedTimeScale;
	}

	public void ChangeSpeed(float scale) {
		if (scale == 0f)
			paused = true;
		else
			savedTimeScale = scale;
		
		Time.timeScale = scale;
	}
}
