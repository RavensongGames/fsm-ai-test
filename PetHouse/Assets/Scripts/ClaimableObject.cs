﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimableObject : MonoBehaviour {
	public StateController owner;

	public void Claim(StateController controller) {
		controller.inUseObject = this;
		owner = controller;
	}

	public void UnClaim(StateController controller) {
		if (controller.inUseObject == this)
			controller.inUseObject = null;
		
		owner = null;
	}
}
